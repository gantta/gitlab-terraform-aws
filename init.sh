PROJECT_ID="20515499"
TF_USERNAME="gantta"
TF_PASSWORD="<GitlabAccessToken>"
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/gitlab-terraform-aws/state/prod"

terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5